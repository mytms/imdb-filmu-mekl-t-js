var app = angular.module('app', []);
app.controller('search', function($scope, $http) {
    $scope.name = "";
    $scope.year = "";

    $scope.link = function() {
       $http.get("http://www.omdbapi.com/?t=" + $scope.name +"&y=" + $scope.year)
       .then(function(response) {
           $scope.Movie = response.data;
        }, function(response) {
           $scope.Movie = "No response";
        });

    };
    $http.get("http://www.omdbapi.com/?t=batman&y=2005")
       .then(function(response) {
           $scope.batman = response.data;
        });
    $http.get("http://www.omdbapi.com/?t=deadpool")
       .then(function(response) {
           $scope.deadpool = response.data;
        });
    $http.get("http://www.omdbapi.com/?t=frozen")
       .then(function(response) {
           $scope.frozen = response.data;
        });
     $http.get("http://www.omdbapi.com/?t=Suicide Squad")
       .then(function(response) {
           $scope.main = response.data;
        });
});